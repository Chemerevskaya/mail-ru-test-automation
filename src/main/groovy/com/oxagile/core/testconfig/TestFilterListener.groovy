package com.oxagile.core.testconfig

import org.testng.annotations.ITestAnnotation

import java.lang.reflect.Constructor
import java.lang.reflect.Method

class TestFilterListener extends aqa.framework.testconfig.listeners.TestFilterListener {

    @Override
    boolean isTestEnabled(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        filterByStoryIdAndIssueId(testMethod) && smokeTestFilter(testMethod) && mobileDeviceFilter(testMethod)
    }

}
