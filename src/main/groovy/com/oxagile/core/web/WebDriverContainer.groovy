package com.oxagile.core.web

import com.oxagile.core.web.utils.ActionsUtils
import com.oxagile.core.web.utils.AlertUtils
import com.oxagile.core.web.utils.JsUtils
import com.oxagile.core.web.utils.ScreenShotUtils
import com.oxagile.core.web.utils.ScreenUtils
import com.oxagile.core.web.utils.WaitUtils

/**
 * Framework core customization point.
 * Custom extended utils can be used here.
 * */
class WebDriverContainer extends aqa.framework.web.WebDriverContainer {

    ActionsUtils actionsUtils = new ActionsUtils()
    AlertUtils alertUtils = new AlertUtils()
    JsUtils jsUtils = new JsUtils()
    WaitUtils waitUtils = new WaitUtils()
    ScreenShotUtils screenShotUtils = new ScreenShotUtils()
    ScreenUtils screenUtils = new ScreenUtils()

    static WebDriverContainer instance = new WebDriverContainer()

}
