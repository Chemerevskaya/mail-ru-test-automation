package com.oxagile.core.web.pageobject

import com.oxagile.core.web.WebDriverContainer

/**
 * Page functionality customization point
 * */
abstract class AbstractPage extends aqa.framework.web.pageobject.AbstractPage {

    /**
     * Extended DriverContainer
     * */
    static WebDriverContainer getDriverContainer() {
        WebDriverContainer.instance
    }
}
