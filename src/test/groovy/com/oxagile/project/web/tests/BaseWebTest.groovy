package com.oxagile.project.web.tests

import aqa.framework.testconfig.basetests.web.BaseFunctionalTest
import aqa.framework.web.DriverContainerProvider
import com.oxagile.core.web.WebDriverContainer
import org.openqa.selenium.remote.DesiredCapabilities

/**
 * Base Test for WEB test-classes.
 * */
class BaseWebTest extends BaseFunctionalTest {

    @Override
    void setupDriverContainerProvider() {
        DriverContainerProvider.provideDriverContainer = { WebDriverContainer.instance }
    }

    @Override
    void launchDriverContainer(){
        WebDriverContainer.launch()
    }

    @Override
    void setDriverContainerCapabilities(){
        WebDriverContainer wdc = DriverContainerProvider.provideDriverContainer()
        wdc.setCapabilities(DesiredCapabilities.chrome())
    }

}
