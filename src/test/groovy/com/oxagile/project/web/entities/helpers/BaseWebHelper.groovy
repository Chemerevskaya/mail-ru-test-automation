package com.oxagile.project.web.entities.helpers

import aqa.framework.web.pageobject.AbstractHelper
import com.oxagile.project.web.entities.pages.BaseWebPage

class BaseWebHelper extends AbstractHelper {

    BaseWebPage getPage() {
        BaseWebPage.instance
    }
}
