### 1 Launch tests:
##### 1 Run web tests:
`mvn clean test -Dsuite.name=regression -Dweb.base.url=https://test-auto.oxagile.com/ -Dweb.browser.name=chrome`

will run tests from *..\suites\regression.xml* testng xml in *chrome* browser for *https://test-auto.oxagile.com* base url

##### 2 Run api tests:
`mvn clean test -Dsuite.name=api -Dweb.base.url=https://test-api.oxagile.com/`

##### 3 Run mobile tests:
`mvn clean test -Dsuite.name=mobile -Dmobile.app=com.someapp.MainActivity -Dmobile.device.id=3e0f64356fcc3894`

##### 4 Run smarttv tests:
`mvn clean test -Dsuite.name=smarttv -Dsmarttv.device.id=LG_43UJ639V`

### 2 Configuration settings: 
List of configuration parameters (possible to override using maven)
##### 2.1 Framework core configs: 
**_configuration.properties_**

###### 2.1.1 web settings
* `web.base.url = ${web.base.url}` - base url for web tests
* `web.browser.name = chrome` - web browser
* `web.driver.remote.is.enabled = ${web.driver.remote.is.enabled}` - flag for enabling selenium grid test run; `false` for local run 
* `web.driver.remote.hub = ${web.driver.remote.hub}` - selenium grid hub, e.g. `http://192.168.112.2:4444/wd/hub`

###### 2.1.2 mobile settings
* `mobile.app = com.someapp.MainActivity` - mobile app for test (APP_ACTIVITY or BUNDLE_ID or BROWSER_NAME)
* `mobile.app.package.name = some.mobile.app.package.name` - android only (APP_PACKAGE)
* `mobile.device.id = 9c7db47dc21e3d3b264f5366273d421efb45d4f2` - mobile device udid
* `mobile.devices.json.path = data/input/common/devices.json` - path to `devices.json`
* `mobile.driver.remote.is.enabled = true` - flag for enabling remote appium test run
* `mobile.driver.remote.hub =  ${mobile.driver.remote.hub}` - remote appium hub, e.g. `http://192.168.42.105:4723/wd/hub`
* `mobile.app.force.install =  false` - `false` if test application installed manually before test

###### 2.1.3 smarttv settings
* `smarttv.device.id = ${smarttv.device.id}` - smarttv device udid
* `smarttv.app = ${smarttv.app}` - smarttv app for test, e.g. `com.domain.app`
* `smarttv.devices.json.path = data/input/common/smarttv-devices.json` - path to `smarttv-devices.json`
* `smarttv.app.force.install = false` - `false` if test application installed manually before test
* `smarttv.platform.cli.path = ${smarttv.platform.cli.path}` - path to platform cli tools for bash, e.g. `/home/username/webos/CLI/bin/`

##### 2.2 Html Reporter configs: 
**_htmlreporter.properties_**
* `jira.url = https://jira.oxagile.com/` - url to project JIRA
* `stack.trace.marker = com.oxagile` - for short stacktrace view
* `xray.sprints.data.path = data/input/common/sprints.json` - sprints data for XRAY report
* `xray.not.automated.link = https://wiki.host.com/display/../not-covered` - some wiki page with not covered filters
* `test.report.theme = default` - ui theme for html test-report. 
Available themes: _default_, _dark_, _light_. For default theme it's possible to define table-header hex color: _default:#505050_
##### 2.3 Logging configs:
**_log4j.properties_**

Possible to setup logs for report portal, html-reporter and console, e. g.:
```
log4j.rootLogger=DEBUG, rp, htmlreporter, console

log4j.appender.rp=com.epam.ta.reportportal.log4j.appender.ReportPortalAppender
log4j.appender.rp.layout=org.apache.log4j.PatternLayout
log4j.appender.rp.layout.ConversionPattern = %d  %-5p  - %m %n

log4j.appender.htmlreporter=aqa.framework.reporter.log4j.appender.AqaFrameworkReportAppender

log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.Threshold=INFO
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern = %d %-5p - %m %n

log4j.logger.rp.org = ERROR
log4j.logger.org.apache.http = OFF
log4j.logger.org.apache.commons = OFF
```

##### 2.4 Report-Portal configs:
**_reportportal.properties_**

Most of configs should be downloaded from report portal project setup page.
Example:
```
rp.tags=;
rp.launch=${launch_name}
rp.project=hybrid-template
rp.uuid=<uuid from rp>
rp.endpoint=http\://192.168.32.111\:8080
```

* `rp.launch=${launch_name}` - report portal launch name (to group launches, e.g. smoke, acceptance, regression, etc)

##### 2.5 Define configs in maven profile:
`mvn clean test -Pdefault,customProfile`

Profile sample1 will define _web.base.url_ parameter
```xml
    <profile>
        <id>customProfile</id>
        <properties>
            <web.base.url>https://project-auto.oxagile.com/</web.base.url>
        </properties>
    </profile>
```


### 3 Reports:
##### 3.1 Test HTML Report: 
HTML report with logs, screnshots. 
* `data/output/report/testReport.html`

##### 3.2 XRay Report:
HTML report for test-cases with statuses. 
* `data/output/xray/xrayReport.html`

##### 3.3 Report Portal:
Remote system for store and manage automation tests' results
* `http://<your_report_portal_host>/ui/#<your_test_project>/launches/all` -> search by launch name

### 4 Setup devices

##### 4.1 Mobile Android/iOs: 
* Modify `data/input/common/devices.json`
* Specify your devices in json format
```
{
  "owner": "oxagile",
  "model": "samsung galaxy s7",
  "name": "SM-G930FD",
  "platform": "ANDROID",
  "platformVersion": "7.0",
  "id": "ad051703b884e931c6", //device udid
  "type": "PHONE",
  "screenFactor": 1,
  "mac":"8C:F5:A3:89:A6:1C", //optional type anything
  "invent":"101949" //optional type anything
},
{
  "owner": "oxagile",
  "model": "ipad 4",
  "name": "ipad4-100543",
  "platform": "IOS",
  "platformVersion": "10.3.3",
  "id": "d0d34654971710a06d34cdb65c86b226c492e9e6", //device udid
  "type": "TABLET",
  "screenFactor": 2, // 2 if retina
  "mac":"A8:88:08:97:8F:AB", //optional type anything
  "invent":"100543" //optional type anything
},
```

##### 4.2 Smart TV WebOS/Tizen: 
* Modify `data/input/common/smarttv-devices.json`
* Specify your devices in json format
```
...
{
  "name": "LG LH604V",
  "platform": "WEBOS", //WEBOS or TIZEN
  "platformVersion": "3.3",
  "id": "LG_LH604V" //device id, get it using cli
},
{
  "name": "LG emulator",
  "platform": "WEBOS",
  "platformVersion": "3.0",
  "id": "emulator"
},
{
  "name": "Samsung UE32M5500AU",
  "platform": "TIZEN",
  "platformVersion": "3.0",
  "id": "UE32M5500AU-112.11"
},
{
  "name": "Tizen emulator",
  "platform": "TIZEN",
  "platformVersion": "4.0",
  "id": "emulator-26101"
}
...
```

### 5 Rerun not passed tests
Due to different reasons 1-5% of UI tests may me flaky. (It means the test failed by unexpected reason and PASSED after rerun).
To handle this cases it's possible to rerun only not passed tests. After each launch XML with not passed tests generated.
See `data/output/retry.xml`

To execute retry xml:
* copy retry XML file to suites directory `data/output/retry.xml` -> `suites/retry.xml`
* set property `-Dsuite.name=retry` in command line

For example executed `acceptance.xml` with `3000` tests. For some reasons `100` tests not passed. And after running `retry.xml` only `20` tests not passed. It means than only `20` not passed tests should be reviewed and analyzed.